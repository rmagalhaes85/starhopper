#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "celestial.h"
#include "database.h"
#include "utils.h"

typedef struct config {
  int verbose;
  right_ascension_t *origin_ra;
  declination_t *origin_dec;
  right_ascension_t *dest_ra;
  declination_t *dest_dec;
  char *origin_name;
  char *origin_id;
  char *dest_name;
  char *dest_id;
  float apperture;
  caldate_t caldate;
  lct_t lct;
  pos_t pos;
} config_t;

void test1() {
  caldate_t cd;
  lct_t lct;
  pos_t fortaleza_pos;
  star_t vega;
  observable_t vega_ob;
  /* config_t config = { 0, "a" }; */
  /* config.origin_ra = "b"; */
  cd = make_caldate(2024, 5, 30);
  lct = make_lct(21, 0, 0);
  fortaleza_pos = make_pos_lat_lng(-3.7327, -38.527);
  fill_star(&vega, "HIP 91262", "Vega", 0.03, make_eq_coords_dms_a(18, 36, 56.34, 38, 47, 1.28));
  fill_observable_star(&vega_ob, &vega, cd, lct, fortaleza_pos);
  /* void get_dms_string(const dms_t dms, char *buffer, size_t buf_size); */
  char buf[16];
  dms_t dec_dms = decimal_to_dms(vega.eq_coords.dec.value);
  get_dms_string(dec_dms, buf, 16);
  /* print_observable_summary(&vega_ob); */
}

void test2() {
  body_t *body;
  body_t *origin_body, *target_body;
  body_list_t *body_list;
  body_list = (body_list_t *) malloc(sizeof(body_list_t));
  if (body_list == NULL) {
    fprintf(stderr, "error allocating celestial bodies list\n");
    exit(1);
  }
  bsc5_get_bodies(body_list);
/* void bsc5_find_body_by_name(const body_list_t *body_list, const char *name, body_t **body) { */
  body = bsc5_find_body_by_name(body_list, "Alp LyrBD+38");
  if (body != NULL) {
    printf("Star name is: %s\n", body->star->name);
  } else {
    printf("Star not found\n");
  }
/* bsc5_get_bodies_and_select_points(body_list_t *body_list_t,  */
/*                                       const char *origin_name, const char *target_name, */
/*                                       body_t **origin_body, body_t **target_body); */
  bsc5_get_bodies_and_select_points(body_list, "BD+44", "Alp LyrBD+38",
                                    &origin_body, &target_body);
}

void test3() {
  char buf[30];
  dms_t dms = { -2, 3, 3 };
  hms_t hms = { 10, 59, 5.23565 };
  get_dms_string(dms, buf, 30);
  printf("DMS: %s\n", buf);
  get_hms_string(hms, buf, 30);
  printf("HMS: %s\n", buf);
}

void test4() {
  char *untrimmed;
  char trimmed[100];
  char *cases[] = {"abc d", "abc d  ", "    abc     ", "   a ", 0};
  int i = 0;
  size_t buf_size;
  int res;
  buf_size = 100;
  while (*(cases + i)) {
    res = trim(*(cases + i), trimmed, &buf_size);
    printf("\"%s\" = (%d) \"%s\"\n", *(cases + i), res, trimmed);
    i++;
  }
  buf_size = 2;
  res = trim("aaa ", trimmed, &buf_size);
  printf("\"%s\" = (%d) \"%s\"\n", "aaa ", res, trimmed);
}

int main(int argc, char *argv[]) {
  /* test1(); */
  test2();
  return 0;
}
