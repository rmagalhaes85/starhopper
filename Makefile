CFLAGS += -std=c89 -O0 -g
LDFLAGS = -lm
OBJS = utils.o celestial.o main.o database.o
EXE = starhopper

.PHONY: all
all: $(OBJS)
	$(CC) $(CFLAGS) $+ -o $(EXE) $(LDFLAGS)

database.o: database.h celestial.h
celestial.o: celestial.c celestial.h
main.o: main.c celestial.o celestial.h utils.o utils.h

.PHONY: clean
clean:
	@rm -rf $(OBJS) $(EXE)

.PHONY: ctags
ctags:
	ctags -R -e
