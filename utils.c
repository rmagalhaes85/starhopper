#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "utils.h"

char *strncpy_or_abort(char *dest, const char *src, size_t n) {
  size_t src_len = strlen(src);
  if ((src_len + 1) > n) {
    errno = ENOBUFS;
    perror("strncpy_or_abort");
    exit(1);
  }
  return strncpy(dest, src, n);
}

int trim(const char *untrimmed, char *trimmed, size_t *buf_size) {
  char *begin, *end;
  int len = strlen(untrimmed);
  int res_len, res;
  char *untrimmed_copy = (char *) malloc(sizeof(char) * len + 1);
  if (untrimmed_copy == NULL) {
    errno = ENOMEM;
    perror("trim: error allocating untrimmed copy buffer");
  }
  strcpy(untrimmed_copy, untrimmed);
  begin = untrimmed_copy;
  end = untrimmed_copy + len;
  while (*begin != '\0' && *begin == ' ') begin++;
  while (end > begin && *(end-1) == ' ') *(--end) = '\0';
  res_len = strlen(begin);
  if (res_len < *buf_size) {
    strncpy(trimmed, begin, *buf_size);
    res = 0;
  } else {
    *buf_size = res_len;
    res = -1;
  }
  free(untrimmed_copy);
  return res;
}
