#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <stddef.h>

#include "celestial.h"
#include "utils.h"

static double compute_frac_time_adj_from_pos(const pos_t pos) {
  return pos.lng / 15;
}

static double compute_time_adj_from_pos(const pos_t pos) {
  return round(compute_frac_time_adj_from_pos(pos));
}

static ut_t lct_to_ut(const lct_t lct, const unsigned char dst, pos_t pos) {
  double value = lct.value;
  /* adjust for daylight saving time if needed */
  if (dst) value--;
  value -= compute_time_adj_from_pos(pos);
  if (value < 0) {
    value += 24;
  } else if (value >= 24) {
    value -= 24;
  }
  return (ut_t) { .value = value };
}

static int is_gregorian(const caldate_t caldate) {
  char buf[10];
  memset(buf, 0, 10);
  snprintf(buf, 9, "%04d%02d%02d", caldate.year, caldate.month, (int) caldate.day);
  return (strcmp(buf, "15821015") < 0) ? 0 : 1;
}

/* reference: Celestial Calculations, pg 41 */
static julian_t caldate_to_julian(const caldate_t caldate) {
  julian_t julian;
  double t, a, b;
  double y, m;
  if (caldate.month > 2) {
    y = caldate.year;
    m = caldate.month;
  } else {
    y = caldate.year - 1;
    m = caldate.month + 12;
  }
  t = (caldate.year < 0) ? 0.75 : 0;
  if (is_gregorian(caldate)) {
    a = (int) y / 100;
    b = 2 - a + ((int) (a/4));
  } else {
    a = b = 0;
  }
  julian.value = b + ((int) (365.25 * y - t)) + ((int) (30.6001 * (m + 1))) + caldate.day + 1720994.5;
  return julian;
}

static gst_t ut_to_gst(const caldate_t caldate, const ut_t ut) {
  caldate_t zero_ut_caldate = caldate;
  julian_t jd = caldate_to_julian((caldate_t) {.year = caldate.year,
                                                 .month = caldate.month,
                                                 .day = (int) caldate.day});
  julian_t jd0 = caldate_to_julian((caldate_t) {.year = caldate.year,
                                                  .month = 1,
                                                  .day = 0});
  double days = jd.value - jd0.value;
  double t = (jd0.value - 2415020) / 36525;
  double r = 6.6460656 + 2400.051262 * t + 0.00002581 * t * t;
  double b = 24 - r + 24 * (caldate.year - 1900);
  double t0 = 0.0657098 * days - b;
  gst_t gst = {.value = t0 + 1.002738 * ut.value};
  if (gst.value < 0) {
    gst.value += 24;
  } else if (gst.value > 24) {
    gst.value -= 24;
  }
  return gst;
}

static lst_t gst_to_lst(const gst_t gst, const pos_t pos) {
  double adj = compute_frac_time_adj_from_pos(pos);
  lst_t lst = { .value = gst.value + adj };
  if (lst.value < 0) { lst.value += 24; }
  else if (lst.value >= 24) { lst.value -= 24; }
  return lst;
}

static hour_angle_t right_ascension_to_hour_angle(const right_ascension_t ra, const lst_t lst) {
  return (hour_angle_t) { .value = lst.value - ra.value };
}

double hms_to_decimal(hms_t hms) {
  return hms.h + (hms.m / 60.) + (hms.s / 3600.);
}

double hms_to_decimal_a(double h, double m, double s) {
  return hms_to_decimal((hms_t) { .h=h, .m=m, .s=s });
}

double dms_to_decimal(dms_t dms) {
  return dms.d + (dms.m / 60.) + (dms.s / 3600.);
}

double dms_to_decimal_a(double d, double m, double s) {
  return dms_to_decimal((dms_t) { .d=d, .m=m, .s=s });
}

static double sind(double x) {
  return sin(x * M_PI / 180);
}

static double cosd(double x) {
  return cos(x * M_PI / 180);
}

static double asind(double x) {
  return asin(x) * 180 / M_PI;
}

static double acosd(double x) {
  return acos(x) * 180 / M_PI;
}

static horizontal_coords_t equatorial_to_horizontal(const caldate_t caldate,
                                                    const lct_t lct,
                                                    const pos_t pos,
                                                    const equatorial_coords_t eq_coord) {
  ut_t ut;
  gst_t gst;
  lst_t lst;
  hour_angle_t ha;
  double ha_deg, phi, dec, h, t0, t1, t2, az;
  ut = lct_to_ut(lct, 0, pos);
  gst = ut_to_gst(caldate, ut);
  lst = gst_to_lst(gst, pos);
  ha = right_ascension_to_hour_angle(eq_coord.ra, lst);
  ha_deg = ha.value * 15;
  phi = pos.lat;
  dec = eq_coord.dec.value;
  t0 = sind(dec) * sind(phi) + cosd(dec) * cosd(phi) * cosd(ha_deg);
  h = asind(t0);
  t1 = sind(dec) - sind(phi) * sind(h);
  t2 = t1 / (cosd(phi) * cosd(h));
  az = acosd(t2);
  az = (sind(ha_deg) > 0) ? 360 - az : az;
  return (horizontal_coords_t) { .az = az, .el = h };
}

static char *get_equatorial_coods_str(const equatorial_coords_t eq) {
  right_ascension_t ra = eq.ra;
  declination_t dec = eq.dec;
  char *res;
  hms_t ra_hms = decimal_to_hms(ra.value);
  dms_t dec_dms = decimal_to_dms(dec.value);
  res = (char *) malloc(40);
  if (res == NULL) {
    errno = ENOMEM;
    perror("get_equatorial_coods_str");
    abort();
  }
}

hms_t decimal_to_hms(double value) {
  hms_t res;
  double a = value;
  res.h = (int) a;
  a -= res.h;
  res.m = (int) (a * 60);
  a -= res.m;
  res.s = (int) (a * 60);
  return res;
}

dms_t decimal_to_dms(double value) {
  dms_t res;
  double a = value;
  res.d = (int) a;
  a -= res.d;
  res.m = (int) (a * 60);
  a -= res.m;
  res.s = (int) (a * 60);
  return res;
}

void get_dms_string(const dms_t dms, char *buffer, size_t buf_size) {
  /* format: +dd° dd' dd.dd" */
  const size_t min_buf = 16;
  if (buf_size < min_buf) {
    errno = ENOMEM;
    perror("get_dms_string: please increase buffer");
    exit(1);
  }
  snprintf(buffer, buf_size, "%c%03d° %02d\' %05.2f\"",
           dms.d > 0 ? '+' : '-', (int) abs(dms.d), (int) dms.m, dms.s);
}

void get_hms_string(const hms_t hms, char *buffer, size_t buf_size) {
  /* format: hh:mm:ss.ss */
  const size_t min_buf = 14;
  if (buf_size < min_buf) {
    errno = ENOMEM;
    perror("get_hms_string: please increase buffer");
    exit(1);
  }
  snprintf(buffer, buf_size, "%02dh %02dm %05.2fs",
           abs(hms.h), (int) hms.m, hms.s);
}

equatorial_coords_t make_eq_coords_dms_a(double ra_d, double ra_m, double ra_s,
                                         double dec_d, double dec_m, double dec_s) {
  right_ascension_t ra;
  declination_t dec;
  equatorial_coords_t eq;
  ra.value = dms_to_decimal_a(ra_d, ra_m, ra_s);
  dec.value = dms_to_decimal_a(dec_d, dec_m, dec_s);
  eq.ra = ra;
  eq.dec = dec;
  return eq;
}

caldate_t make_caldate(double year, double month, double day) {
  return (caldate_t) { .year = year, .month = month, .day = day};
}

lct_t make_lct(double h, double m, double s) {
  return (lct_t) { .value =  hms_to_decimal_a(h, m, s) };
}

pos_t make_pos_lat_lng(double lat, double lng) {
  return (pos_t) { .lat = lat, .lng = lng };
}

void fill_star(star_t *star, const char *hipparcos_id, const char *name, float mag, equatorial_coords_t eq_coords) {
  strncpy_or_abort(star->hipparcos_id, hipparcos_id, CEL_SM_SIZE);
  strncpy_or_abort(star->name, name, CEL_MD_SIZE);
  star->mag = mag;
  star->eq_coords = eq_coords;
}

void fill_observable_star(observable_t *observable, const star_t *star,
                          const caldate_t cd, const lct_t lct, const pos_t pos) {
  observable->type = BODY_STAR;
  observable->planet = NULL;
  observable->star = star;
  observable->cd = cd;
  observable->lct = lct;
  observable->pos = pos;
  observable->hor_coords = equatorial_to_horizontal(cd, lct, pos,
                                                    star->eq_coords);
}

void fill_observable_planet(observable_t *observable, const planet_t *planet,
                            const caldate_t cd, const lct_t lct, const pos_t pos) {
  fprintf(stderr, "fill_observable_planet: not implemented\n");
  exit(1);
}

static body_t *alloc_body() {
  body_t *res;
  res = (body_t *) malloc(sizeof(body_t));
  if (res == NULL) {
    errno = ENOMEM;
    perror("alloc_body");
    exit(1);
  }
  return res;
}

body_t *alloc_star_body() {
  body_t *res;
  res = alloc_body();
  res->star = (star_t *) malloc(sizeof(star_t));
  if (res->star == NULL) {
    errno = ENOMEM;
    perror("alloc_star_body");
    exit(1);
  }
  return res;
}

body_t *alloc_planet_body() {
  body_t *res;
  res = alloc_body();
  res->planet = (planet_t *) malloc(sizeof(planet_t));
  if (res->planet == NULL) {
    errno = ENOMEM;
    perror("alloc_planet_body");
    exit(1);
  }
  return res;
}

void free_body(body_t *body) {
  if (body == NULL) return;
  if (body->star != NULL) free(body->star);
  if (body->planet != NULL) free(body->planet);
  free(body);
  body = NULL;
}

/* void generate_observable_summary(const observable_t *observable, char *buffer, size_t buf_size) { */
/*   size_t outstanding_bytes = buf_size; */
/*   char tmp_buf[100]; */
/* } */

void print_observable_summary(const observable_t *observable) {
  char name[64];
  char id[64];
  char type[64];
  memset(name, 0, 64);
  memset(id, 0, 64);
  memset(type, 0, 64);
  switch(observable->type) {
  case BODY_STAR:
    strncpy_or_abort(name, observable->star->name, 64);
    strncpy_or_abort(id, observable->star->hipparcos_id, 64);
    strncpy_or_abort(type, "STAR", 64);
    break;
  case BODY_PLANET:
    /* strncpy_or_abort(name, observable->star->name, 64); */
    /* strncpy_or_abort(id, observable->star->hipp_id, 64); */
    strncpy_or_abort(type, "PLANET", 64);
    break;
  default:
    fprintf(stderr, "print_observable_summary: unknown celestial body type\n");
    exit(1);
  }
  /*
    type
id
name
ra (alpha) / dec (delta)
observational parameters in (pos), (calendar date) and (time):
azimuth / elevation
*/
  exit(1);
}
