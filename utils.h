#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

char *strncpy_or_abort(char *dest, const char *src, size_t n);
int trim(const char *untrimmed, char *trimmed, size_t *buf_size);

/* UTILS_H */
#endif
