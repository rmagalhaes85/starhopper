#ifndef CELESTIAL_H
#define CELESTIAL_H

#include <stddef.h>

#define CEL_SM_SIZE (16)
#define CEL_MD_SIZE (64)

#if !defined(__STDC_VERSION__)
#  define __STDC_VERSION__ 0
#endif

#if !defined(M_PI)
#  define M_PI 3.14159265359
#endif

#if (__STDC_VERSION__ < 199901L)
static double round(double value) {
  if (value == 0.) return 0.;
  if (value < 0) return ((int) value) - 1;
  return ((int) value) + 1;
}
#endif

typedef struct hms {
  double h, m, s;
} hms_t;

typedef struct dms {
  double d, m, s;
} dms_t;

typedef struct pos {
  double lat, lng;
} pos_t;

typedef struct lct {
  double value;
} lct_t;

typedef struct ut {
  double value;
} ut_t;

typedef struct gst {
  double value;
} gst_t;

typedef struct lst {
  double value;
} lst_t;

/* calendar date. NOTICE THAT, in this struct, time is expressed as fractions of days */
typedef struct caldate {
  int year;
  int month;
  double day;
} caldate_t;

/* julian day number */
typedef struct julian {
  double value;
} julian_t;

typedef struct declination {
  double value;
} declination_t;

typedef struct right_ascension {
  double value;
} right_ascension_t;

typedef struct equatorial_coords {
  right_ascension_t ra;
  declination_t dec;
} equatorial_coords_t;

typedef struct azimuth {
  double value;
} azimuth_t;

typedef struct elevation {
  double value;
} elevation_t;

typedef struct horizontal_coords {
  azimuth_t az;
  elevation_t el;
} horizontal_coords_t;

typedef struct hour_angle {
  double value;
} hour_angle_t;

typedef struct star {
  char hipparcos_id[CEL_SM_SIZE];
  char name[CEL_MD_SIZE];
  float mag;
  equatorial_coords_t eq_coords;
} star_t;

typedef struct planet {
} planet_t;

typedef struct body {
  star_t *star;
  planet_t *planet;
  struct body *next;
} body_t;

typedef struct body_list {
  int count;
  body_t **bodies;
} body_list_t;

typedef enum { BODY_STAR, BODY_PLANET } body_type;

typedef struct observable {
  body_type type;
  const star_t *star;
  const planet_t *planet;
  caldate_t cd;
  lct_t lct;
  pos_t pos;
  horizontal_coords_t hor_coords;
} observable_t;

horizontal_coords_t star_in_horizontal_coords(caldate_t caldate, lct_t lct, pos_t pos, equatorial_coords_t eq);
void init_star(star_t *star, const char hipparcos_id, const char name, float mag, equatorial_coords_t eq_coords);
double hms_to_decimal(hms_t hms);
double hms_to_decimal_a(double h, double m, double s);
double dms_to_decimal(dms_t dms);
double dms_to_decimal_a(double d, double m, double s);
hms_t decimal_to_hms(double value);
dms_t decimal_to_dms(double value);
equatorial_coords_t make_eq_coords_dms_a(double ra_d, double ra_m, double ra_s,
                                   double dec_d, double dec_m, double dec_s);
caldate_t make_caldate(double year, double month, double day);
lct_t make_lct(double h, double m, double s);
pos_t make_pos_lat_lng(double lat, double lng);
void fill_observable_star(observable_t *observable, const star_t *star, 
                          const caldate_t cd, const lct_t lct, const pos_t pos);
void fill_observable_planet(observable_t *observable, const planet_t *planet,
                            const caldate_t cd, const lct_t lct, const pos_t pos);
void fill_star(star_t *star, const char *hipparcos_id, const char *name, float mag,
               equatorial_coords_t eq_coords);
void get_dms_string(const dms_t dms, char *buffer, size_t buf_size);
void get_hms_string(const hms_t hms, char *buffer, size_t buf_size);
void print_observable_summary(const observable_t *observable);
body_t *alloc_star_body();
body_t *alloc_planet_body();
void free_body(body_t *body);

#endif
