#ifndef DATABASE_H
#define DATABASE_H

#include "celestial.h"

int bsc5_get_bodies_and_select_points(body_list_t *body_list_t,
                                      const char *origin_name, const char *target_name,
                                      body_t **origin_body, body_t **target_body);
int bsc5_get_bodies(body_list_t *body_list_t);
body_t *bsc5_find_body_by_name(const body_list_t *body_list, const char *name);

#endif
