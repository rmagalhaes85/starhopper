#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "database.h"
#include "celestial.h"
#include "utils.h"

#define TEMP_BUF_SIZE (50)
#define LINE_BUF_SIZE (200)

static void bsc5_read_name(const char *line, char *dest) {
  static char temp_buffer[TEMP_BUF_SIZE];
  size_t bsize = CEL_MD_SIZE - 1;
  /* make sure that the buffer is large enough to store the field */
  assert(TEMP_BUF_SIZE > 12);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 7, 12);
  trim(temp_buffer, dest, &bsize);
}

static double bsc5_read_ra(const char *line) {
  static char temp_buffer[TEMP_BUF_SIZE];
  hms_t hms;
  /* temp buffer must be larger than the biggest field (declination's arc secs) */
  assert(TEMP_BUF_SIZE > 4);
  hms = (hms_t) {0, 0, 0};
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 75, 2);
  hms.h = atoi(temp_buffer);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 77, 2);
  hms.m = atoi(temp_buffer);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 79, 4);
  hms.s = atof(temp_buffer);
  return hms_to_decimal(hms);
}

static double bsc5_read_dec(const char *line) {
  static char temp_buffer[TEMP_BUF_SIZE];
  dms_t dms;
  /* temp buffer must be larger than the biggest field (declination's arc secs) */
  assert(TEMP_BUF_SIZE > 3);
  dms = (dms_t) {0, 0, 0};
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 83, 3);
  dms.d = atoi(temp_buffer);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 86, 2);
  dms.m = atoi(temp_buffer);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 88, 2);
  dms.s = atoi(temp_buffer);
  return dms_to_decimal(dms);
}

static double bsc5_read_mag(const char *line) {
  static char temp_buffer[TEMP_BUF_SIZE];
  assert(TEMP_BUF_SIZE > 2);
  memset(temp_buffer, 0, TEMP_BUF_SIZE);
  memcpy(temp_buffer, line + 104, 5);
  return atof(temp_buffer);
}

int bsc5_get_bodies_and_select_points(body_list_t *body_list,
                                      const char *origin_name, const char *target_name,
                                      body_t **origin_body, body_t **target_body)
{
  FILE *fcatalog;
  char line_buffer[LINE_BUF_SIZE];
  size_t n = 0;
  body_t *star_body;
  hms_t hms;
  dms_t dms;
  int i;
  char *res;
  body_t *first, *last;
  body_list->count = 0;
  body_list->bodies = NULL;
  fcatalog = fopen("./catalog", "r");
  if (fcatalog == NULL) {
    perror("get_bodies: error opening catalog file");
    exit(1);
  }
  i = 0;
  while (!feof(fcatalog)) {
    memset(line_buffer, 0, LINE_BUF_SIZE);
    res = fgets(line_buffer, LINE_BUF_SIZE, fcatalog);
    if (feof(fcatalog))
      break;
    if (res == NULL) {
      fprintf(stderr, "get_bodies: error reading a catalog file line\n");
      fclose(fcatalog);
      exit(1);
    }
    star_body = alloc_star_body();
    bsc5_read_name(line_buffer, star_body->star->name);
    star_body->star->eq_coords.ra.value = bsc5_read_ra(line_buffer);
    star_body->star->eq_coords.dec.value = bsc5_read_dec(line_buffer);
    star_body->star->mag = bsc5_read_mag(line_buffer);
    if (origin_body && !strcmp(star_body->star->name, origin_name)) {
      *origin_body = star_body;
    } else if (target_body && !strcmp(star_body->star->name, target_name)) {
      *target_body = star_body;
    }
    if (body_list->count == 0) {
      first = star_body;
      last = star_body;
    } else {
      last->next = star_body;
      last = star_body;
    }
    body_list->count++;
  }
  body_list->bodies = &first;
  fclose(fcatalog);
}

int bsc5_get_bodies(body_list_t *body_list)
{
  return bsc5_get_bodies_and_select_points(body_list, NULL, NULL, NULL, NULL);
}

body_t *bsc5_find_body_by_name(const body_list_t *body_list, const char *name) {
  body_t *i;
  for (i = *body_list->bodies; i != NULL; i = i->next) {
    if (i->star != NULL) {
      if (strcmp(i->star->name, name) == 0) {
        return i;
      }
    } else if (i->planet != NULL) {
      fprintf(stderr, "bsc5_find_body_by_name: planets are not yet supported\n");
      exit(1);
    } else {
      fprintf(stderr, "bsc5_find_body_by_name: unsupported celestial body type\n");
      exit(1);
    }
  }
  return NULL;
}

int bsc5_get_usable_bodies(body_list_t *body_list,
                           const char *origin_name, const char *target_name,
                           body_t **origin_body, body_t **target_body,
                           caldate_t caldate, lct_t lct, pos_t pos)
{
  // - bsc5_get_bodies_and_select_points, using the passed arguments
  // - validate that both origin and target were marked by the previous function call
  // - compute azimuthal coords for origin and target and make sure they are both above horizon.
  //   (horizon being defined by config, default might be an elevation of 10 degrees)
  // - compute the midpoint between origin and target (midpoint will be in equatorial coords)
  // - filter the list of bodies, leaving only stars within the circle defined by:
  //   - center in the midpoint between origin and target
  //   - radius equals to half the line connecting origin and target + X degrees
  //     (X being defined by config, default might be 5 degrees)
  // - compute horizontal/azimuthal coords for the stars remaining after previous filtering
  // - remove from the previous list all the stars located below horizon
  // - return remaining stars. These will be used to build the graph and apply pathfinding algorithms
  return 0;
}
